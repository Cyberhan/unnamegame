<?php
//有道翻译API
define("CURL_TIMEOUT",   20);
define("URL",            "https://openapi.youdao.com/api"); 
define("APP_KEY",        "41b5668a8e443b08"); //替换为您的应用ID
define("SEC_KEY",        "fBSXDvvgTk5nyZ25mdZkIoeGDXz1IWK2");//替换为您的密钥
class Common_model extends CI_Model 
{
    public function __construct()
    {
    	parent::__construct();
        $this->load->database();
        date_default_timezone_set('PRC');//设置时区
    }
    public function limitView($page,$data = null)
    {
        $limitData = $data;
        $limitData['pageName'] = $page;
        $this->load->view($page,$limitData);
    }
    /**
     * 返回当前年份
     * @return string 当前年份
     */
    public function getYear()
    {
    	return date('Y');
    }
    /**
     * 返回当前月份
     * @return string 当前月份
     */
    public function getMonth()
    {
    	return date('m');
    }
    /**
     * 返回当前日期
     * @return string 当前日期
     */
    public function getDay()
    {
    	return date('d');
    }
    /**
     * 返回当前年月日 小时-分钟-秒
     * @return string 当前年月日 小时-分钟-秒
     */
    public function getDate()
    {
		return date('Y-m-d H:i:s');
    }
    //有道翻译API
    public function CNtoJP($query)
    {
        //return $this->translate("good","EN","zh-CHS");
        return $this->translate($query, "zh-CHS", "ja")["translation"][0];
    }
    public function JPtoCN($query)
    {
       //return $this->translate("good","EN","zh-CHS");
        return $this->translate($query,"ja" , "zh-CHS")["translation"][0];
    }
    //翻译入口
    function translate($query, $from, $to)
    {
        $args = array(
            'q' => $query,
            'appKey' => APP_KEY,
            'salt' => rand(10000,99999),
            'from' => $from,
            'to' => $to,
        );
        $args['sign'] = $this->buildSign(APP_KEY, $query, $args['salt'], SEC_KEY);
        $ret = $this->call(URL, $args);
        $ret = json_decode($ret, true);
        return $ret; 
    }

    //加密
    function buildSign($appKey, $query, $salt, $secKey)
    {/*{{{*/
        $str = $appKey . $query . $salt . $secKey;
        $ret = md5($str);
        return $ret;
    }/*}}}*/

    //发起网络请求
    function call($url, $args=null, $method="post", $testflag = 0, $timeout = CURL_TIMEOUT, $headers=array())
    {/*{{{*/
        $ret = false;
        $i = 0; 
        while($ret === false) 
        {
            if($i > 1)
                break;
            if($i > 0) 
            {
                sleep(1);
            }
            $ret = $this->callOnce($url, $args, $method, false, $timeout, $headers);
            $i++;
        }
        return $ret;
    }/*}}}*/

    function callOnce($url, $args=null, $method="post", $withCookie = false, $timeout = CURL_TIMEOUT, $headers=array())
    {/*{{{*/
        $ch = curl_init();
        if($method == "post") 
        {
            $data = $this->convert($args);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_POST, 1);
        }
        else 
        {
            $data = $this->convert($args);
            if($data) 
            {
                if(stripos($url, "?") > 0) 
                {
                    $url .= "&$data";
                }
                else 
                {
                    $url .= "?$data";
                }
            }
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(!empty($headers)) 
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if($withCookie)
        {
            curl_setopt($ch, CURLOPT_COOKIEJAR, $_COOKIE);
        }
        $r = curl_exec($ch);
        curl_close($ch);
        return $r;
    }/*}}}*/

    function convert(&$args)
    {/*{{{*/
        $data = '';
        if (is_array($args))
        {
            foreach ($args as $key=>$val)
            {
                if (is_array($val))
                {
                    foreach ($val as $k=>$v)
                    {
                        $data .= $key.'['.$k.']='.rawurlencode($v).'&';
                    }
                }
                else
                {
                    $data .="$key=".rawurlencode($val)."&";
                }
            }
            return trim($data, "&");
        }
        return $args;
    }
}

?>