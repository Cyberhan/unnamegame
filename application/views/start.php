<html style="overflow: hidden;">
<head>
	<meta charset="UTF-8">
	<title><?php echo $title; ?></title>
	<link rel="shortcut icon" href="#" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- 载入layui CSS -->
	<link rel="stylesheet" href="<?php echo base_url('/layui/css/layui.css');?>"  media="all">

</head>
<body id="body" style="position:relative; overflow: hidden;">
</body>


<!-- 载入layui JS -->
<script src="<?php echo base_url('/layui/layui.js');?>"></script>
<!-- 载入各种支持库 -->
<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<!-- 载入md5插件 -->
<script src="<?php echo base_url('/')."md5.js";?>"></script>
<!-- 载入LimitControl控制模块 -->
<script src="<?php echo base_url('/')."LimitControl.js";?>"></script>
<!-- 模块组入口 -->
<script src="<?php echo base_url('/'.$projectName.'/module_index/module_index.js');?>"></script>
</html>